package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Server struct {
	addr         string
	router       http.Handler
	readTimeout  time.Duration
	writeTimeout time.Duration
}

func NewServer(addr string, router http.Handler, readTimeout, writeTimeout time.Duration) *Server {
	return &Server{
		addr:         addr,
		router:       router,
		readTimeout:  readTimeout,
		writeTimeout: writeTimeout,
	}
}

func (s *Server) HandleHTTP() {
	server := &http.Server{
		Addr:         s.addr,
		Handler:      s.router,
		ReadTimeout:  s.readTimeout,
		WriteTimeout: s.writeTimeout,
	}

	log.Printf("Server listening on %s\n", s.addr)
	log.Fatal(server.ListenAndServe())
}

func NewRouter() http.Handler {
	router := http.NewServeMux()
	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Hello, World!"))
	})
	return router
}

func main() {

	server := NewServer("localhost:8080", NewRouter(), 10*time.Second, 10*time.Second)

	server.HandleHTTP()
}

var client *mongo.Client

type Item struct {
	ID    primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Name  string             `json:"name"`
	Price int                `json:"price"`
}

type Order struct {
	ID    primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Items []Item             `json:"items"`
	Total int                `json:"total"`
}

func main() {
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	var err error
	client, err = mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Conexão com MongoDB estabelecida")

	http.HandleFunc("/cart", handleCart)
	http.HandleFunc("/orders", handleOrders)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func handleCart(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		getCart(w, r)
	case "POST":
		addToCart(w, r)
	case "DELETE":
		clearCart(w, r)
	}
}

func handleOrders(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		getOrders(w, r)
	case "POST":
		finalizeOrder(w, r)
	}
}

func getCart(w http.ResponseWriter, r *http.Request) {
	var cart []Item
	collection := client.Database("restaurant").Collection("cart")
	cur, err := collection.Find(context.TODO(), bson.M{})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {
		var item Item
		err := cur.Decode(&item)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		cart = append(cart, item)
	}
	if err := cur.Err(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(cart)
}

func addToCart(w http.ResponseWriter, r *http.Request) {
	var item Item
	json.NewDecoder(r.Body).Decode(&item)
	collection := client.Database("restaurant").Collection("cart")
	_, err := collection.InsertOne(context.TODO(), item)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	json.NewEncoder(w).Encode(item)
}

func clearCart(w http.ResponseWriter, r *http.Request) {
	collection := client.Database("restaurant").Collection("cart")
	_, err := collection.DeleteMany(context.TODO(), bson.M{})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	json.NewEncoder(w).Encode("Carrinho limpo")
}

func finalizeOrder(w http.ResponseWriter, r *http.Request) {
	var cart []Item
	collection := client.Database("restaurant").Collection("cart")
	cur, err := collection.Find(context.TODO(), bson.M{})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer cur.Close(context.TODO())

	total := 0
	for cur.Next(context.TODO()) {
		var item Item
		err := cur.Decode(&item)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		cart = append(cart, item)
		total += item.price
	}
	if err := cur.Err(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	order := Order{
		Items: cart,
		Total: total,
	}
	ordersCollection := client.Database("restaurant").Collection("orders")
	_, err = ordersCollection.InsertOne(context.TODO(), order)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	_, err = collection.DeleteMany(context.TODO(), bson.M{})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(order)
}

func getOrders(w http.ResponseWriter, r *http.Request) {
	var orders []Order
	collection := client.Database("restaurant").Collection("orders")
	cur, err := collection.Find(context.TODO(), bson.M{})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {
		var order Order
		err := cur.Decode(&order)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		orders = append(orders, order)
	}
	if err := cur.Err(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(orders)
}
